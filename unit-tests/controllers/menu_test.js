(function() {
    'use strict';

    describe('opentele.controllers.menu module', function() {

        beforeEach(module('opentele.controllers.menu'));
        beforeEach(module('opentele.stateServices'));

        describe('MenuCtrl positive', function() {
            var scope, appContext, messageThreads, runController,
                nativeService, videoService, restServiceResult, pluginRegistry;

            beforeEach(module(function($provide) {

                restServiceResult = {
                    unreadMessages: 3
                };

                videoService = {
                    restartPendingConferencePolling: function(videoConference) {}
                };
                $provide.value('videoService', videoService);

                nativeService = {
                    clientIsVideoEnabled: function(callback) {
                        callback({enabled: true});
                    }
                };
                $provide.value('nativeService', nativeService);

                messageThreads = {
                    unreadMessages: function(user, onSuccess) {
                        onSuccess(restServiceResult);
                    }
                };
                $provide.value('messageThreads', messageThreads);
            }));

            beforeEach(inject(function($rootScope, $controller, _appContext_,
                                       _headerService_, _messageThreads_,
                                       _nativeService_, _videoService_,
                                       _pluginRegistry_) {
                appContext = _appContext_;
                pluginRegistry = _pluginRegistry_;
                appContext.currentUser.set({
                    links: {
                        videoPendingConference: 'fake URL #1',
                        patientHasPendingMeasurement: 'fake URL #2',
                        measurementFromPatient: 'fake URL #3'
                    }
                });
                scope = $rootScope.$new();
                runController = function() {
                    $controller('MenuCtrl', {
                        '$scope': scope,
                        'appContext': _appContext_,
                        'headerService': _headerService_,
                        'messageThreads': _messageThreads_,
                        'nativeService': _nativeService_,
                        'videoService': _videoService_,
                        'pluginRegistry': _pluginRegistry_
                    });
                };
            }));

            it('should have a $scope', function() {
                runController();

                expect(scope).toBeDefined();
            });

            it('should have menuItems', function() {
                runController();

                expect(scope.model.menuItems).toBeDefined();
            });

            it('should disable menu items if user has no link to area', function () {
                runController();

                expect(scope.model.menuItems.length).toBe(0);
            });

            it('should enable menu items if user has link to area', function () {
                appContext.currentUser.set({
                    links: {
                        questionnaires: "bla",
                        messageThreads: "bla",
                        unreadMessages: "bla",
                        password: "bla"
                    }
                });
                runController();

                expect(scope.model.menuItems.length).toBe(3);
                expect(menuItemsContains(scope.model.menuItems, "#/perform_measurements")).toBeTruthy();
                expect(menuItemsContains(scope.model.menuItems, "#/messages")).toBeTruthy();
                expect(menuItemsContains(scope.model.menuItems, "#/change_password")).toBeTruthy();
            });

            it('should be able to show menu item from plugin', function() {
                pluginRegistry.registerPlugin({
                    menuItem: {
                        name: 'My plugin',
                        url: '#/plugin',
                        enabled: function() {return true;}
                    }
                });

                runController();

                expect(menuItemsContains(scope.model.menuItems, "#/plugin")).toBeTruthy();
            });

            it('should call plugin menu item added handler if defined', function () {
                var addedHandlerCalled = false;
                pluginRegistry.registerPlugin({
                    menuItem: {
                        name: 'My plugin',
                        url: '#/plugin',
                        enabled: function() {return true;},
                        added: function() {addedHandlerCalled = true;}
                    }
                });

                runController();

                expect(addedHandlerCalled).toBeTruthy();
            });

            it ('should not show menu item for plugin if it is disabled', function() {
                pluginRegistry.registerPlugin({
                    menuItem: {
                        name: 'My plugin',
                        url: '#/plugin',
                        enabled: function() {return false;}
                    }
                });

                runController();

                expect(menuItemsContains(scope.model.menuItems, "#/plugin")).toBeFalsy();
            });

            it ('should call video service if client video is enabled', function() {
                spyOn(videoService, "restartPendingConferencePolling");

                runController();

                expect(videoService.restartPendingConferencePolling).toHaveBeenCalled();
            });

            var menuItemsContains = function(menuItems, url) {
                for (var i = 0; i < menuItems.length; i++) {
                    if (menuItems[i].url === url) {
                        return true;
                    }
                }

                return false;
            };
        });
    });
}());
