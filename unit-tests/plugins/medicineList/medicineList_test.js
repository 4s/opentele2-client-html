(function() {
    'use strict';

	describe('MedicineListCtrl', function () {
		var scope, timeout, templateCache, pdfDelegate, appContext, navigationMock, runController;

		beforeEach(module('opentele.medicineList'));

		beforeEach(module(function($provide) {
			navigationMock = {
				zoomIn: function() {},
				zoomOut: function() {},
				next: function() {},
				prev: function() {}
			};
			spyOn(navigationMock, 'zoomIn');
			spyOn(navigationMock, 'zoomOut');
			spyOn(navigationMock, 'next');
			spyOn(navigationMock, 'prev');

			var pdfDelegateMock = jasmine.createSpy();
			pdfDelegateMock.$getByHandle = function() {return navigationMock;};

            var summaryServiceMock = {
                get: function(user, onSuccess) {
                    onSuccess({
                        links: {
                            medicineList: "details_url"
                        }
                    });
                },
                authorizationHeaderValue: function() {return 'secret';}
            };

            $provide.value('medicineListSummary', summaryServiceMock);
			$provide.value('pdfDelegate', pdfDelegateMock);
		}));

		beforeEach(inject(function($rootScope, $controller, $timeout, $templateCache, $window, _appContext_, _pdfDelegate_, _medicineListSummary_) {
			scope = $rootScope.$new();
            timeout = $timeout;
            templateCache = $templateCache;
			appContext = _appContext_;
			pdfDelegate = _pdfDelegate_;

			var user = {
				links: {
					medicineListSummary: "summary_url"
				}
			};
			appContext.currentUser.set(user);

            templateCache.put('plugins/medicineList/pdfViewer.html', "<div>hello</div>");

			runController = function() {
				$controller('MedicineListCtrl', {
					$scope: scope,
                    $timeout: timeout,
					$window: $window,
					$templateCache: templateCache,
					appContext: appContext,
					pdfDelegate: pdfDelegate,
                    medicineListSummary: _medicineListSummary_
				});
			};
		}));

		it('should show medicine list from user links', function () {
			runController();

			expect(scope.model.medicineList).toEqual("details_url");
		});

        it('should load pdf viewer html from template cache', function () {
            spyOn(templateCache, 'get').andCallThrough();
            runController();

            expect(templateCache.get).toHaveBeenCalledWith('plugins/medicineList/pdfViewer.html');
            expect(scope.model.pdfViewerHtml).toBe("<div>hello</div>");
        });

		it('should set authorization header for pdf viewer', function () {
			runController();

			expect(scope.model.headers).toEqual({authorization: 'secret'});
		});

		it('should delegate zoom controls and page navigation to pdf viewer', function () {
			runController();

			scope.zoomIn();
			scope.zoomOut();
			scope.next();
			scope.prev();

			expect(navigationMock.zoomIn).toHaveBeenCalled();
			expect(navigationMock.zoomOut).toHaveBeenCalled();
			expect(navigationMock.prev).toHaveBeenCalled();
			expect(navigationMock.next).toHaveBeenCalled();
		});

        it('should temporarely disable buttons to allow redraw when clicked', function() {
            runController();

            scope.zoomIn();
			scope.zoomOut();
			scope.next();
			scope.prev();

            expect(scope.model.disableZoomIn).toBeTruthy();
            expect(scope.model.disableZoomOut).toBeTruthy();
            expect(scope.model.disablePrev).toBeTruthy();
            expect(scope.model.disableNext).toBeTruthy();

            timeout.flush();

            expect(scope.model.disableZoomIn).toBeFalsy();
            expect(scope.model.disableZoomOut).toBeFalsy();
            expect(scope.model.disablePrev).toBeFalsy();
            expect(scope.model.disableNext).toBeFalsy();
        });
	});
}());
