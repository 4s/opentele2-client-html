(function () {
    'use strict';

    var videoService = angular.module('opentele.restApiServices.video', [
        'ngResource',
        'opentele.stateServices'
    ]);

    videoService.service('videoService', function ($http, $location, $q, $timeout, appContext) {

        var config = {
            cache: false,
            silentRequest: true
        };

        var conferenceCanceler = null;

        var pollForPendingConference = function (videoConference) {
            console.log("Start: pollForPendingConference");

            videoConference.roomKey = undefined;
            videoConference.serviceUrl = undefined;

            var onSuccess = function(conference) {
                if (conferenceCanceler !== null) {
                    conferenceCanceler.reject();
                }
                conferenceCanceler = null;
                videoConference.roomKey = conference.roomKey;
                videoConference.serviceUrl = conference.serviceUrl;

                if (videoConference.roomKey !== undefined &&
                    videoConference.serviceUrl !== undefined) {
                    appContext.requestParams.set('videoConference', videoConference);
                    $location.path('/joinConference');
                } else {
                    pollForPendingConference(videoConference);
                }
            };

            var onError = function(_, status) {
                console.log("Status:" + status);
                if(status === 0) {
                  return;
                }

                if(status === 401) {
                  appContext.requestParams.set('authenticationError', 'LOGGED_OUT');
                  $location.path('/login');
                  return;
                }
                $timeout(function() { pollForPendingConference(videoConference); }, 2000);
            };

            conferenceCanceler = $q.defer();
            config.timeout = conferenceCanceler.promise;
            config.errorPassThrough = true;
            config.allowTimeout = true;
            var pendingConferenceUrl = videoConference.pendingConferenceUrl;
            $http.get(pendingConferenceUrl, config)
              .success(onSuccess)
              .error(onError);
        };

        var cancelPendingConferencePolling = function() {
            if (conferenceCanceler === null) {
                return;
            }

            conferenceCanceler.resolve();
        };

        var restartPendingConferencePolling = function(videoConference) {
            cancelPendingConferencePolling();
            pollForPendingConference(videoConference);
        };

        var pollForPendingMeasurement = function(pendingMeasurementUrl, onSuccess) {
            $http.get(pendingMeasurementUrl, config).success(onSuccess);
        };

        var sendMeasurementFromPatient = function(measurementFromPatientUrl,
                                                  measurement, onSuccess, onError) {
            var config = {
                errorPassThrough: true
            };
            $http.post(measurementFromPatientUrl, measurement, config)
                .success(onSuccess)
                .error(onError);
        };

        return {
            pollForPendingConference: pollForPendingConference,
            cancelPendingConferencePolling: cancelPendingConferencePolling,
            restartPendingConferencePolling: restartPendingConferencePolling,
            pollForPendingMeasurement: pollForPendingMeasurement,
            sendMeasurementFromPatient: sendMeasurementFromPatient
        };

    });
}());
