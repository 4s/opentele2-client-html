(function() {
    'use strict';

    var header = angular.module('opentele.controllers.header', [
        'opentele-commons.nativeServices'
    ]);

    header.controller('HeaderCtrl', function ($scope, $location, nativeService) {

        $scope.goMainMenu = function () {
            $scope.$parent.$broadcast('menuClick');
            $location.path('/menu');
        };

        $scope.goBack = function () {
            $scope.$parent.$broadcast('backClick');
        };

        $scope.goLogout = function () {
            $location.path('/login');
        };

    });
}());
