(function() {
    'use strict';

    var menu = angular.module('opentele.controllers.menu', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices',
        'opentele.translations',
        'opentele-commons.nativeServices'
    ]);

    menu.config(function($routeProvider) {
        $routeProvider.when('/menu', {
            title: 'MENU',
            templateUrl: 'areas/menu/menu.html'
        });
    });

    menu.controller('MenuCtrl', function($scope, $location, $translate,
                                         appContext, headerService,
                                         messageThreads, nativeService,
                                         videoService, pluginRegistry) {

        headerService.setHeader(true);
        headerService.setMainMenu(false);
        headerService.setBack(false);

        var user = appContext.currentUser.get();
        $scope.model = {};

        var hasLinkRelation = function(relation) {
            return user.hasOwnProperty('links') && user.links.hasOwnProperty(relation);
        };

        var updateUnreadMessages = function(menuItem) {
            messageThreads.unreadMessages(user, function(data) {
                menuItem.name = menuItem.name + " (" + data.unreadMessages + ")";
            });
        };

        var setupVideoConferencePoller = function(user) {
            if (hasLinkRelation('videoPendingConference')) {

                nativeService.clientIsVideoEnabled(function(response) {
                    if (response.enabled === false) {
                        return;
                    }

                    var videoConference = {
                        username: user.username,
                        pendingConferenceUrl: user.links.videoPendingConference,
                        pendingMeasurementUrl: user.links.patientHasPendingMeasurement,
                        measurementFromPatientUrl: user.links.measurementFromPatient
                    };

                    videoService.restartPendingConferencePolling(videoConference);
                });
            }
        };

        var menuItems = [];
        if (hasLinkRelation('questionnaires')) {
            menuItems.push({
                url: "#/perform_measurements",
                name: $translate.instant("MENU_PERFORM_MEASUREMENTS")
            });
        }
        if (hasLinkRelation('messageThreads')) {
            var menuItem = {
                url: "#/messages",
                name: $translate.instant("MENU_MESSAGES")
            };
            menuItems.push(menuItem);
            updateUnreadMessages(menuItem);
        }
        if (hasLinkRelation('acknowledgements')) {
            menuItems.push({
                url: "#/acknowledgements",
                name: $translate.instant("MENU_ACKNOWLEDGEMENTS")
            });
        }
        if (hasLinkRelation('measurements')) {
            menuItems.push({
                url: "#/my_measurements",
                name: $translate.instant("MENU_MY_MEASUREMENTS")
            });
        }
        if (hasLinkRelation('linksCategories')) {
            menuItems.push({
                url: "#/links_categories",
                name: $translate.instant("MENU_LINKS_CATEGORIES")
            });
        }
        pluginRegistry.plugins.forEach(function(plugin) {
            var menuItem = plugin.menuItem || null;
            if (menuItem !== null && menuItem.enabled(user)) {
                menuItems.push(menuItem);
                if (typeof menuItem.added === 'function') {
                    menuItem.added(user, menuItem);
                }
            }
        });
        if (hasLinkRelation('password')) {
            menuItems.push({
                url: "#/change_password",
                name: $translate.instant("MENU_CHANGE_PASSWORD")
            });
        }

        $scope.model.menuItems = menuItems;

        setupVideoConferencePoller(user);
    });
}());
