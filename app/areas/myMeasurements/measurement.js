(function() {
    'use strict';

    var measurement = angular.module('opentele.controllers.measurement', [
        'ngRoute',
        'opentele.stateServices',
        'opentele.restApiServices',
        'opentele.plotServices'
    ]);

    measurement.config(function($routeProvider) {
        $routeProvider.when('/measurement', {
            title: 'MEASUREMENT_TITLE',
            templateUrl: 'areas/myMeasurements/measurement.html'
        });
    });

    measurement.controller('MeasurementCtrl', function($scope, $window, $location, appContext,
                                                       measurements, headerService,
                                                       plotService, filters, utils) {

        $scope.$on('backClick', function() {
            $window.history.back();
        });

        $scope.showPopup = function(measurement, event) {
            // TODO: ideally this should be looser coupled to what is happening in the DOM.
            var POPUP_WIDTH = 480;
            var POPUP_HEIGHT = 220;
            $scope.model.popupMeasurement = measurement;
            $scope.model.showPopup = true;
            $scope.model.popupOffsetX = event.pageX - POPUP_WIDTH / 2;
            $scope.model.popupOffsetY = event.pageY - POPUP_HEIGHT / 2;
        };

        $scope.hidePopup = function() {
            $scope.model.showPopup = false;
        };

        $scope.showFilter = function(filter) {
            retrieveMeasurements($scope.model, filter);
        };

        function performHorribleBloodPressureHack(model, filter, currentMeasurement) {

            function mergeMeasurements(bloodPressureMeasurements,
                                       pulseMeasurements) {

                var mergedMeasurements = {
                    "type": utils.types.BLOOD_PRESSURE,
                    "unit": "mmHg, BPM",
                    "measurements": []
                };

                var i = 0;
                var j = 0;
                while (i < bloodPressureMeasurements.length && j < pulseMeasurements.length) {

                    var bloodPressureMeasurement = bloodPressureMeasurements[i];
                    var pulseMeasurement = pulseMeasurements[j];
                    if ((new Date(bloodPressureMeasurement.timestamp)).getTime() >
                        (new Date(pulseMeasurement.timestamp)).getTime()) {
                        mergedMeasurements.measurements.push(bloodPressureMeasurement);
                        i++;

                    } else if ((new Date(bloodPressureMeasurement.timestamp)).getTime() ===
                               (new Date(pulseMeasurement.timestamp)).getTime()) {
                        var newMeasurement = bloodPressureMeasurement;
                        newMeasurement.measurement.pulse = pulseMeasurement.measurement;
                        mergedMeasurements.measurements.push(newMeasurement);
                        i++;
                        j++;

                    } else {
                        j++;
                    }
                }

                if (i < bloodPressureMeasurements.length) {
                    mergedMeasurements.measurements =
                        mergedMeasurements.measurements.concat(
                            bloodPressureMeasurements.slice(i));
                }

                return mergedMeasurements;
            }

            var bloodPressureURL = model.measurement.links.measurement;
            var pulseName = utils.types.PULSE;
            var pulseURL = bloodPressureURL.substring(0, bloodPressureURL.lastIndexOf('/') + 1) + pulseName;
            var pulseMeasurementRef = {
                "name": pulseName,
                "links": {
                    "measurement": pulseURL
                }
            };

            measurements.get(pulseMeasurementRef, filter, function(extraPulseMeasurement) {

                var bloodPressureMeasurements = currentMeasurement.measurements;
                var pulseMeasurements = extraPulseMeasurement.measurements;
                var mergedMeasurements = mergeMeasurements(bloodPressureMeasurements, pulseMeasurements);
                render(model, mergedMeasurements, filter);
            });
        }

        function render(model, currentMeasurement, filter) {
            plotService.setGraphAndTableFlags(model, currentMeasurement.type);
            plotService.renderFilters(model);
            plotService.renderTables(model, currentMeasurement);
            plotService.renderGraphs(model, currentMeasurement, filter);
        }

        var retrieveMeasurements = function(model, filter) {
            if (filter === undefined || filter === null) {
                filter = filters.WEEK;
            }
            model.filter = filter;

            measurements.get(model.measurement, filter, function(currentMeasurement) {

                if (model.measurement.name === 'MEASUREMENT_TYPE_' +
                    utils.types.BLOOD_PRESSURE.toUpperCase()) {
                    performHorribleBloodPressureHack(model, filter, currentMeasurement);
                } else {
                    render(model, currentMeasurement, filter);
                }
            });
        };

        headerService.setBack(true);
        if (!appContext.requestParams.containsKey('selectedMeasurement')) {
            $location.path('/menu');
            return;
        }

        $scope.model = {};
        $scope.model.measurement = appContext.requestParams.getAndClear('selectedMeasurement');
        retrieveMeasurements($scope.model);
    });
}());
