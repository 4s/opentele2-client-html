(function () {
    'use strict';

    var ScenarioHelper = function () {
        this.waitFor = function(promiseFn, testFn) {
            browser.wait(function () {
                var deferred = protractor.promise.defer();
                promiseFn.then(function (data) {
                    testFn(data, deferred);
                });
                return deferred.promise;
            });
        };
        
        this.containsCssClass = function(allClasses, classToMatch) {
            return allClasses.split(' ').indexOf(classToMatch) !== -1;
        };
        
        this.getCssClasses = function(element) {
            return element.getAttribute('class');
        };
    };

    module.exports = new ScenarioHelper();
} ());
