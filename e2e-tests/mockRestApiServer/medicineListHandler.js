(function() {
    'use strict';

	var fs = require('fs');

    exports.summary = function(req, res, baseUrl) {
        var summary = {
            "uploadDate": new Date(),
            "isNew": true,
            "links": {
                "medicineList": baseUrl + "patient/medicinelist"
            }
        };

        res.send(summary);
    };

	exports.get = function(req, res, baseUrl) {
		var file = __dirname + '/files/some_medicine_list.pdf';
		//res.header('Content-disposition', 'inline; filename=medicine-list.pdf');
		//res.setHeader('content-disposition', 'attachment; filename=medicine-list.pdf');
		res.setHeader('content-type', 'application/pdf');
		fs.createReadStream(file).pipe(res);
	};
}());
